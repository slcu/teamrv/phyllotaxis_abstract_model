import numpy as np
from numba import njit
from model import simulate_and_save

output_folder = "output_bifurcation_diagram_20230406"
primordia_size_progression = [2**-0.5,2**0.5] # geometric mean of 1, arithmetic mean of 1.06
primordia_size_scaling_function = "lambda age: 0.1+0.9*age/(1+age)"
primordium_size_scaling_function_evaluated = njit(eval(primordia_size_scaling_function))
# calculate primordia_size_scaling_function_values
dt = 0.125
rmax = 100
meristem_radius=10
meristem_growth_rate = 1
age_max = (rmax-meristem_radius)/meristem_growth_rate
ages = np.arange(0,age_max+dt,step=dt)
primordium_size_scaling_function_values = primordium_size_scaling_function_evaluated(ages).astype(np.float64)

for gamma in np.arange(3,0.35-0.025,-0.025):# simulate from gamma of 3 to 0.35
    simulate_and_save(gamma,
                      simultaneous_primordia=False,#difference from 20230406
                      dt=dt,meristem_radius=10,
                      primordia_size_progression=primordia_size_progression,
                      precompute_primordium_size_scaling_function_values=True,
                      primordium_size_scaling_function=primordia_size_scaling_function,
                      primordium_size_scaling_function_values =primordium_size_scaling_function_values,
                      output_folder=output_folder,
                      output_filename=f"gamma_{gamma}")
    print(gamma)