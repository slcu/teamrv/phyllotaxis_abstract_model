import sys
import json
import numpy as np
import datetime #for naming output files
from matplotlib import animation
from matplotlib import pyplot as plt
from numba import njit

def simulate_and_save(gamma=1.2,
                      active_ring_resolution=3600,
                      conicity=3, # must be greater than 0; 1 for flat plane 
                      dt=0.125, # 1/8 - a power of 2 so should reduce floating point errors
                      full_output=True,
                      initial_state=None,
                      meristem_radius=10,
                      output_animation=True,
                      output_filename=None, # Should not include file extension. If None, outputs will be named according to current time
                      output_folder="output",
                      potential_threshold=1,
                      precompute_primordium_size_scaling_function_values=True, # overrides specification of primordium_size_scaling_function_values if False
                      primordia_size_progression=None, # list of relative sizes of each sequential primordium generated. Primordia generated at the same time have the same size 
                      primordium_size_scaling_function=None, # lambda function string that determines primordium size scaling as a function of age
                      primordium_size_scaling_function_values=None, # array - if specified, prevents evaluation of primdorium_size_scaling_function. primdorium_size_scaling_function should still be specified for recording in the output file but this is not essential
                      resolution_dpi=None,
                      simultaneous_primordia=False,
                      stiffness=3,
                      tmax=200):
    """
    Note that this function uses eval, so should not be used on unsanitised input
    """
    
    alternating_primordia_sizes = primordia_size_progression is not None
    age_dependent_primordium_size_scaling = (primordium_size_scaling_function is not None) or (
        (primordium_size_scaling_function_values is not None) and precompute_primordium_size_scaling_function_values)
    
    # define calculate_potential function
    if alternating_primordia_sizes: # define calculate_potential to take primordia_sizes as an input
        if age_dependent_primordium_size_scaling:
            if precompute_primordium_size_scaling_function_values: # calculate primordium_size_scaling_function_values array and access it in calculate_potential  
                # compute primordium_size_scaling_function_values if none have already been provided
                if primordium_size_scaling_function_values is None:
                    # evaluate primordium_size_scaling_function for every possible primordium age value, to be looked up by the calculate_potential function
                    primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function))
                    age_max = (rmax-meristem_radius)/meristem_growth_rate
                    ages = np.arange(0,age_max+dt,step=dt)
                    primordium_size_scaling_function_values = primordium_size_scaling_function_evaluated(ages).astype(np.float64) # must not be np.int32 type as this causes errors when attempting to store in JSON
                    
                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions,primordia_sizes): # primordia_positions must not be empty
                    potential = 0.0
                    for i in range(len(primordia_sizes)):
                        r2,theta2 = primordia_positions[i]
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        primordium_size = primordia_sizes[i]
                        age_index = int(primordium_age/dt)
                        scaled_primordium_size = primordium_size*primordium_size_scaling_function_values[age_index]
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential
            else: # call primordium_size_scaling_function_evaluated inside calculate_potential rather than precomputing the possible outputs
                primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function)) # njit is important to allow the function to be accessed by other njitted functions as well as improving performance
                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions,primordia_sizes): # primordia_positions must not be empty
                    potential = 0.0
                    for i in range(len(primordia_sizes)):
                        r2,theta2 = primordia_positions[i]
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        primordium_size = primordia_sizes[i]
                        scaled_primordium_size = primordium_size*primordium_size_scaling_function_evaluated(primordium_age)
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential
        else:
            @njit # decorator for numba just-in-time compiler in nopython mode
            def calculate_potential(r,theta,primordia_positions,primordia_sizes): # primordia_positions must not be empty
                potential = 0
                for i in range(len(primordia_sizes)):
                    r2,theta2 = primordia_positions[i]
                    primordium_size = primordia_sizes[i]
                    diff_theta = theta - theta2
                    d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                    if d != 0:
                        potential += (primordium_size/d)**stiffness
                    else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                        return 9999
                return potential
    else: # define calculate_potential to use primordium_radius instead of taking primordia_sizes as an input  
        if age_dependent_primordium_size_scaling: 
            if precompute_primordium_size_scaling_function_values: # calculate primordium_size_scaling_function_values array and access it in calculate_potential   
                # compute primordium_size_scaling_function_values if none have already been provided
                if primordium_size_scaling_function_values is None:
                    # evaluate primordium_size_scaling_function for every possible primordium age value, to be looked up by the calculate_potential function
                    primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function))
                    age_max = (rmax-meristem_radius)/meristem_growth_rate
                    ages = np.arange(0,age_max+dt,step=dt)
                    primordium_size_scaling_function_values = primordium_size_scaling_function_evaluated(ages).astype(np.float64) # must not be np.int32 type as this causes errors when attempting to store in JSON

                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions): # primordia_positions must not be empty
                    potential = 0.0
                    for r2,theta2 in primordia_positions:
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        age_index = int(primordium_age/dt)
                        scaled_primordium_size = primordium_radius*primordium_size_scaling_function_values[age_index]
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential

            else: # call primordium_size_scaling_function_evaluated inside calculate_potential rather than precomputing the possible outputs
                primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function)) # njit is important to allow the function to be accessed by other njitted functions as well as improving performance
                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions): # primordia_positions must not be empty
                    potential = 0.0
                    for r2,theta2 in primordia_positions:
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        scaled_primordium_size = primordium_radius*primordium_size_scaling_function_evaluated(primordium_age)
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential
        else: # simplest model

            @njit # decorator for numba just-in-time compiler in nopython mode
            def calculate_potential(r,theta,primordia_positions): # primordia_positions must not be empty
                potential = 0
                for r2,theta2 in primordia_positions:
                    diff_theta = theta - theta2
                    d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                    if d != 0:
                        potential += (primordium_radius/d)**stiffness
                    else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                        return 9999
                return potential
    
    # record time of current run for output file name
    time = str(datetime.datetime.now()).replace(" ","_").replace(":","-").replace(".","_")

    # time discretisation
    ts = np.arange(0,tmax,dt)

    # define boundary beyond which points are removed from the simulation
    rmax = 100
    
    # calculate primordium radius
    primordium_radius = gamma * meristem_radius * (conicity ** -0.5)

    # initialise active ring (meristem)
    meristem_angles = np.linspace(0,2*np.pi,active_ring_resolution, endpoint = False)
    meristem_potentials = np.zeros(len(meristem_angles))
    meristem_growth_rate = 1 # not a parameter of the model as it has no effect on behaviour
    
    if initial_state is None:
        initial_state = [(meristem_radius,0)] # overwritten if corresponding initial state is present in file
    
    # add initial primordia and initialise history variables
    initial_primordia_angles = [theta for r,theta in initial_state]
    history_new_primordia_times = [0.0 for pos in initial_state]
    history_new_primordia_angles = list(initial_primordia_angles)
    num_primordia_initiated = len(initial_state)
    history_num_primordia_initiated = [num_primordia_initiated]
    primordia_positions = np.array(initial_state,
                                dtype="float64") # must be initialised as an array of floats, never ints, to allow values to be incremented by float )
    history_primordia_positions = [primordia_positions] # used for plotting
    
    if not alternating_primordia_sizes: # do not track primordia sizes
        
        # main loop
        for t in ts:
            
            meristem_potentials = np.array([ calculate_potential(meristem_radius,angle,primordia_positions)
                                            for angle in meristem_angles]) # potential value for each point in the active ring
            below_potential_threshold = meristem_potentials < potential_threshold # boolean value for each point in the active ring based on whether it is below the threshold potential

            # generate new primordia
            if any(below_potential_threshold):
                if not simultaneous_primordia:
                    while any( below_potential_threshold ):
                        candidate_new_primordium_numbers = np.where(meristem_potentials == meristem_potentials.min())[0] # list of indices of active ring points that have the lowest potential, which must be below potential_threshold because otherwise the while loop would not have continued. We have to index the 0th element of the resulting tuple, see here for discussion: https://stackoverflow.com/questions/50646102/what-is-the-purpose-of-numpy-where-returning-a-tuple
                        new_primordium_number = np.random.choice(candidate_new_primordium_numbers) # choose randomly between all positions that have the minimum potential
                        new_primordium_angle = meristem_angles[new_primordium_number]
                        new_primordium_position = [[meristem_radius,new_primordium_angle]]
                        # record angle and time and add new primordium
                        history_new_primordia_times.append(t)
                        history_new_primordia_angles.append(new_primordium_angle)
                        primordia_positions = np.append(primordia_positions,new_primordium_position,axis=0)
                        num_primordia_initiated += 1
                        # update potentials and calculate whether there remain any points on the active ring that are below the potential threshold
                        meristem_potentials = np.array([ calculate_potential(meristem_radius,angle,primordia_positions)
                                                        for angle in meristem_angles]) # potential value for each point in the active ring
                        below_potential_threshold = meristem_potentials < potential_threshold # boolean value for each point in the active ring based on whether it is below the threshold potential
                else: # simultaneous primordia
                    # initiate new primordia at all points on the active ring where the potential energy is the minimum value
                    new_primordium_numbers = np.where(meristem_potentials == meristem_potentials.min())[0] # list of indices of active ring points that have the lowest potential, which must be below potential_threshold because otherwise the while loop would not have continued. We have to index the 0th element of the resulting tuple, see here for discussion: https://stackoverflow.com/questions/50646102/what-is-the-purpose-of-numpy-where-returning-a-tuple
                    for new_primordium_number in new_primordium_numbers:
                        new_primordium_angle = meristem_angles[new_primordium_number]
                        new_primordium_position = [[meristem_radius,new_primordium_angle]]
                        # record angle and time and add new primordium
                        history_new_primordia_times.append(t)
                        history_new_primordia_angles.append(new_primordium_angle)
                        primordia_positions = np.append(primordia_positions,new_primordium_position,axis=0)
                        num_primordia_initiated += 1
                    # update potentials
                    meristem_potentials = np.array([ calculate_potential(meristem_radius,angle,primordia_positions)
                                                    for angle in meristem_angles]) # potential value for each point in the active ring
                history_num_primordia_initiated.append(num_primordia_initiated)
                    
            # filter out out-of-bounds primordia
            inside_bounds = np.apply_along_axis(lambda pos: pos[0] <= rmax,1, primordia_positions)# Boolean array of whether each primordium is inside the bounds
            primordia_positions = primordia_positions[inside_bounds]
            
            # update primordia positions
            for i in range(len(primordia_positions)):
                r,theta = primordia_positions[i]
                new_position = np.array([r+meristem_growth_rate*dt,theta])
                primordia_positions[i] = new_position 
                    
            history_primordia_positions.append(primordia_positions)
        
    else: # track primordia sizes
        
        # initialise size variables
        primordia_sizes = primordium_radius * np.ones(len(initial_state)) # all initial primordia have the same size
        primordia_size_progression_original = primordia_size_progression
        primordia_size_progression = primordium_radius * np.array(primordia_size_progression_original)
        history_primordia_sizes = [primordia_sizes]
        
        # main loop
        for t in ts:
            meristem_potentials = np.array([ calculate_potential(meristem_radius,angle,primordia_positions,primordia_sizes)
                                            for angle in meristem_angles]) # potential value for each point in the active ring
            below_potential_threshold = meristem_potentials < potential_threshold # boolean value for each point in the active ring based on whether it is below the threshold potential

            # generate new primordia
            if any(below_potential_threshold):
                new_primordia_size = primordia_size_progression[num_primordia_initiated%len(primordia_size_progression)] # Modular division with the length of the primordia_size_progression list allows the list to be indexed in a loop - going back to the start once you reach the end
                if not simultaneous_primordia:
                    while any( below_potential_threshold ):
                        candidate_new_primordium_numbers = np.where(meristem_potentials == meristem_potentials.min())[0] # list of indices of active ring points that have the lowest potential, which must be below potential_threshold because otherwise the while loop would not have continued. We have to index the 0th element of the resulting tuple, see here for discussion: https://stackoverflow.com/questions/50646102/what-is-the-purpose-of-numpy-where-returning-a-tuple
                        new_primordium_number = np.random.choice(candidate_new_primordium_numbers) # choose randomly between all positions that have the minimum potential
                        new_primordium_angle = meristem_angles[new_primordium_number]
                        new_primordium_position = [[meristem_radius,new_primordium_angle]]
                        # record angle and time and add new primordium
                        history_new_primordia_times.append(t)
                        history_new_primordia_angles.append(new_primordium_angle)
                        primordia_positions = np.append(primordia_positions,new_primordium_position,axis=0)
                        primordia_sizes = np.append(primordia_sizes,[new_primordia_size],axis=0)
                        num_primordia_initiated += 1
                        # update potentials and calculate whether there remain any points on the active ring that are below the potential threshold
                        meristem_potentials = np.array([ calculate_potential(meristem_radius,angle,primordia_positions,primordia_sizes)
                                                        for angle in meristem_angles]) # potential value for each point in the active ring
                        below_potential_threshold = meristem_potentials < potential_threshold # boolean value for each point in the active ring based on whether it is below the threshold potential
                else: # simultaneous primordia
                    # initiate new primordia at all points on the active ring where the potential energy is the minimum value
                    new_primordium_numbers = np.where(meristem_potentials == meristem_potentials.min())[0] # list of indices of active ring points that have the lowest potential, which must be below potential_threshold because otherwise the while loop would not have continued. We have to index the 0th element of the resulting tuple, see here for discussion: https://stackoverflow.com/questions/50646102/what-is-the-purpose-of-numpy-where-returning-a-tuple
                    for new_primordium_number in new_primordium_numbers:
                        new_primordium_angle = meristem_angles[new_primordium_number]
                        new_primordium_position = [[meristem_radius,new_primordium_angle]]
                        # record angle and time and add new primordium
                        history_new_primordia_times.append(t)
                        history_new_primordia_angles.append(new_primordium_angle)
                        primordia_positions = np.append(primordia_positions,new_primordium_position,axis=0)
                        primordia_sizes = np.append(primordia_sizes,[new_primordia_size],axis=0)
                        num_primordia_initiated += 1
                    # update potentials
                    meristem_potentials = np.array([ calculate_potential(meristem_radius,angle,primordia_positions,primordia_sizes)
                                                    for angle in meristem_angles]) # potential value for each point in the active ring
                history_num_primordia_initiated.append(num_primordia_initiated)
                    
            # filter out out-of-bounds primordia
            inside_bounds = np.apply_along_axis(lambda pos: pos[0] <= rmax,1, primordia_positions)# Boolean array of whether each primordium is inside the bounds
            primordia_sizes = primordia_sizes[inside_bounds]
            primordia_positions = primordia_positions[inside_bounds]
            
            # update primordia positions
            for i in range(len(primordia_positions)):
                r,theta = primordia_positions[i]
                new_position = np.array([r+meristem_growth_rate*dt,theta])
                primordia_positions[i] = new_position 
                    
            history_primordia_positions.append(primordia_positions)
            history_primordia_sizes.append(primordia_sizes)
   
    #save run output
    if output_filename is None: 
        filepath = f'{output_folder}/{time}_PhysicalPrimordiaModelOutput.json'
    else:
        filepath = f'{output_folder}/{output_filename}.json'

    # all data structures are converted to nested lists to facilitate serialisation
    data = {"gamma":gamma,
            "initial_state":initial_state,
            "potential_threshold":potential_threshold,
            "meristem_radius":meristem_radius,
            "conicity":conicity,
            "stiffness":stiffness,
            "simultaneous_primordia":simultaneous_primordia,
            "alternating_primordia_sizes":alternating_primordia_sizes,
            "age_dependent_primordium_size_scaling":age_dependent_primordium_size_scaling,
            "precompute_primordium_size_scaling_function_values":precompute_primordium_size_scaling_function_values,
            "primordia_size_progression":
                "N/A" if not alternating_primordia_sizes 
                else list(primordia_size_progression_original),
            "primordium_size_scaling_function":
                "N/A" if primordium_size_scaling_function is None
                else primordium_size_scaling_function,
            "tmax":tmax,
            "dt":dt,
            "active_ring_resolution":active_ring_resolution}
    if full_output:
        data["history_num_primordia_initiated"] = history_num_primordia_initiated
        data["history_new_primordia_times"] = history_new_primordia_times
        data["history_new_primordia_angles"] = history_new_primordia_angles
        if not alternating_primordia_sizes:
            data["history_primordia_sizes"] = "N/A"
        else:
            history_primordia_sizes_lists = [list(primordia_sizes_array) for primordia_sizes_array in history_primordia_sizes]
            data["history_primordia_sizes"] = history_primordia_sizes_lists
        if age_dependent_primordium_size_scaling and precompute_primordium_size_scaling_function_values:
            data["primordium_size_scaling_function_values"] = list(primordium_size_scaling_function_values)
        else:
            data["primordium_size_scaling_function_values"] = "N/A"
        history_primordia_positions_lists = [list([[float(r),float(theta)] for r,theta in primordia_positions_array]) for primordia_positions_array in history_primordia_positions]
        data["history_primordia_positions"] = history_primordia_positions_lists
    with open(filepath, 'w+') as f:
        json.dump(data, f)
        
    #save run animation - source: https://stackoverflow.com/questions/46849712/how-can-i-animate-a-set-of-points-with-matplotlib
    if output_animation:
        fig = plt.figure()
        ax = fig.add_subplot(projection='polar')
        coords = history_primordia_positions[0]
        r_coords,theta_coords = np.array(coords).transpose()
        mat, = ax.plot(r_coords, theta_coords, 'o',c="#00cc0a")
        ax.set_rmax(rmax)
        ax.set_rticks([meristem_radius]) # include only a single r gridline to mark the position of the active ring
        ax.set_thetagrids([]) # remove theta gridlines
        ax.set_yticklabels([]) # hide r axis tick labels
        
        def animate(i):
            coords = history_primordia_positions[i]
            r_coords,theta_coords = np.array(coords).transpose()
            mat.set_data(theta_coords,r_coords) # matplotlib takes theta first, r second
            return mat,

        anim = animation.FuncAnimation(fig, animate, frames = len(history_primordia_positions), interval = 100*dt #interval between frames in milliseconds
                                      )
        if output_filename is None:
            movie_filepath = f'{output_folder}/{time}_PhysicalPrimordiaModelMovie.mp4'
        else:
            movie_filepath = f'{output_folder}/{output_filename}.mp4'
        
        if resolution_dpi:
            anim.save(movie_filepath,dpi=resolution_dpi)
        else:
            anim.save(movie_filepath)

        plt.close('all') # once animation is complete, close the figure to stop it occupying memory
    return None
    
if len(sys.argv) > 1:
    print(f'Running simulation with parameters {sys.argv[1:]}')
    simulate_and_save(float(sys.argv[1]),float(sys.argv[2]),float(sys.argv[3]))
else:
    print("simulate_and_save function has been defined and is ready to be run")