import sys
import json
import numpy as np
from matplotlib import animation
from matplotlib import pyplot as plt
from potential_plotter import plot_potentials

np.seterr(over='ignore') # ignore overflow warnings arising from spikes in the potential function leading to numbers larger that can be stored in np.float64, resulting in their conversion to np.inf

def potential_movie(filename,output_filename,potential_plot_max_formula = "2*potential_threshold",
                    frames_start = 0, frames_stop = -1, frames_step = 1,
                    theta_resolution = 1000, r_resolution = 1000, resolution_dpi = 400,
                    write_simulation_parameters_to_video_title_metadata = True,
                    write_resolution_parameters_to_video_comment_metadata = True):
    """
    filename should end in .json
    output_filename should end in a video extension like .mp4
    potential_plot_max_formula - float or string of Python code to be evaluated
        - could use the variables `potential_threshold` or `average_potential`
        - recommended options - "2*potential_threshold", "10*average_potential"
    This function uses eval() with {'__builtins__':None}
    """
    
    # load data from simulation output file
    with open(filename,"r") as f:
        data = json.load(f)
    
    gamma = data["gamma"]
    conicity = data["conicity"]
    meristem_radius = data["meristem_radius"]
    stiffness = data["stiffness"]
    dt = data["dt"]
    potential_threshold = data["potential_threshold"]
    history_primordia_positions = data["history_primordia_positions"]
    
    
    history_primordia_sizes = data["history_primordia_sizes"]
    if history_primordia_sizes == "N/A":
        history_primordia_sizes = [None]*len(history_primordia_positions)
        
    primordium_size_scaling_function_values = data["primordium_size_scaling_function_values"]
    if primordium_size_scaling_function_values == "N/A":
        primordium_size_scaling_function_values = None

    average_potential = None
    if "average_potential" in potential_plot_max_formula:#run the following code only if average_potential is used in the formula to calculate potential_plot_max
        # sampling potentials and scaling
        median_potentials = []
        for i in [0.5,0.6,0.7,0.8,0.9,1]:
            t = int(i* len(history_primordia_positions))-1 # -1 to avoid going beyond range of the list
            positions = history_primordia_positions[t]
            sizes = history_primordia_sizes[t]
            potentials = plot_potentials(positions,primordia_sizes=sizes,data_mode=True,
                                         conicity=conicity,
                                         dt=dt,
                                         gamma=gamma,
                                         meristem_radius=meristem_radius,
                                         precompute_primordium_size_scaling_function_values=True,
                                         primordium_size_scaling_function=None,
                                         primordium_size_scaling_function_values=primordium_size_scaling_function_values,
                                         stiffness=stiffness)
            median_potential = np.nanmedian(potentials) # take the median of the potentials, ignoring NaN values
            median_potentials.append(median_potential)
        average_potential = np.mean(median_potentials)
    
    potential_plot_max_value = float(eval(potential_plot_max_formula,{'__builtins__': None},
                                          {"average_potential":average_potential,"potential_threshold":potential_threshold}))
                                            #local variables that can be accessed by the eval call

    # animation
    fig = plt.figure()
    
    def animate(frame_num):
        positions = history_primordia_positions[frame_num]
        sizes = history_primordia_sizes[frame_num]
        plt.clf() # clear the previous plot
        return plot_potentials(positions,primordia_sizes=sizes,data_mode=False,
                            overlay_positions=True,overlay_active_ring=True,
                            conicity=conicity,
                            dt=dt,
                            gamma=gamma,
                            meristem_radius=meristem_radius,
                            precompute_primordium_size_scaling_function_values=True,
                            primordium_size_scaling_function=None,
                            potential_plot_max=potential_plot_max_value,
                            primordium_size_scaling_function_values=primordium_size_scaling_function_values,
                            stiffness=stiffness,
                            theta_resolution = theta_resolution,r_resolution = r_resolution)

    if frames_stop == -1:
        frames_range = range(frames_start,len(history_primordia_positions),frames_step)
    else:
        frames_range = range(frames_start,frames_stop,frames_step)

    
    # create the animation
    anim = animation.FuncAnimation(fig, animate, frames=frames_range, blit=False,interval = 100*dt*frames_step)
    
    # set the video metadata
    metadata = {}
    if write_simulation_parameters_to_video_title_metadata:
        # get the simulation parameters for the metadata
        metadata["title"]=f'{{ gamma: {data["gamma"]},primordium_size_scaling_function:{data["primordium_size_scaling_function"]},primordia_size_progression:{data["primordia_size_progression"]}}}'
    if write_resolution_parameters_to_video_comment_metadata:
        metadata["comment"]=f'{{ theta_resolution: {theta_resolution},r_resolution:{r_resolution},resolution_dpi:{resolution_dpi}}}'
    
    # save the animation video    
    anim.save(output_filename, dpi=resolution_dpi, metadata=metadata)

def single_filename(filename, potential_plot_max_formula="2*potential_threshold",
                    frames = None,
                    theta_resolution = 1000, r_resolution = 1000, resolution_dpi = 400,
                    write_simulation_parameters_to_video_title_metadata = True,
                    write_resolution_parameters_to_video_comment_metadata = True):
    input_filename=f"output/{filename}.json"
    output_filename=f"Movies/{filename}.mp4"
    potential_movie(input_filename,output_filename, potential_plot_max_formula = potential_plot_max_formula,
                    frames = frames,
                    theta_resolution = theta_resolution, r_resolution = r_resolution, resolution_dpi = resolution_dpi,
                    write_simulation_parameters_to_video_title_metadata = write_simulation_parameters_to_video_title_metadata,
                    write_resolution_parameters_to_video_comment_metadata = write_resolution_parameters_to_video_comment_metadata)

if len(sys.argv) == 2:
    filename = sys.argv[1] # must be the name of the JSON file
    input_filename=f"output/{filename}.json"
    output_filename=f"Movies/{filename}.mp4"
    print(f'Generating movie "{output_filename}" for input file "{input_filename}"')
    potential_movie(input_filename,output_filename)
elif len(sys.argv) > 2 and len(sys.argv) < 4:
    print(f'Generating movie "{sys.argv[2]}" for input file "{sys.argv[1]}"')
    potential_movie(sys.argv[1],sys.argv[2])
elif len(sys.argv) == 4:
    print(f'Generating movie "{sys.argv[2]}" for input file "{sys.argv[1]}" using potential_plot_max_formula "{sys.argv[3]}"')
    potential_movie(sys.argv[1],sys.argv[2])
else:
    print("potential_movie and single_filename functions have been defined and are ready to be run")