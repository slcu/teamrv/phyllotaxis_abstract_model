import numpy as np
from scipy import stats, signal
from glob import glob # function for listing files in a directory matching a particular pattern
import json
from matplotlib import pyplot as plt

def get_modes(values,kernel_bandwidth=0.01):
    """
    Given a list of numerical values, returns the list of modes of the distribution by estimating the probability denisty function (PDF) and finding its maxima
    Small kernel_bandwidth gives high sensitivity
    """
    values = np.array(values)
    if np.all(values == values[0]): 
        return values[0:1] # if all the values are the same, return the first value. Otherwise, stats.gaussian_kde throws an error because the array has zero variance
    
    # estimate the PDF using KDE

    kernel = stats.gaussian_kde(values,bw_method=kernel_bandwidth)

    xs = np.linspace(min(values)*0.9, max(values)*1.1, 1000) # extend slightly beyond the maximum and minimum of the range so that modes at the extremes of the range can be identified
    pdf = kernel(xs)

    # find the local maxima of the PDF
    maxima_indices = signal.argrelextrema(pdf, np.greater)[0]
    modes = xs[maxima_indices]
    
    return modes

directory = "output_bifurcation_diagram_20230406"
raw_filenames = glob(f"{directory}/*.json")
filenames = [filename.replace("\\","/") for filename in raw_filenames]

truncation_time = 150 # time point

plastochrons_modes = []
divergence_angles_modes = []
gammas_corresponding_to_plastochrons_modes = []
gammas_corresponding_to_divergence_angles_modes = []
for filename in filenames:
    try:
        with open(filename) as f:
            data = json.load(f)
        history_new_primordia_times = data["history_new_primordia_times"]
        history_angles = data["history_new_primordia_angles"]
        plastochrons = [history_new_primordia_times[i+1] - history_new_primordia_times[i]
                                for i in range(len(history_new_primordia_times)-1)]
        divergence_angles = [(history_angles[i+1] - history_angles[i])%(2*np.pi) 
                                for i in range(len(history_angles)-1)]
        
        # calculate modes
        dt = data["dt"]
        tmax = data["tmax"]
        if tmax <= truncation_time: # ensure the simulation is sufficiently long to reache quilibrium
            continue
        else:
            gamma = data["gamma"]
            
            # get the index of the first primordium initiated after the truncation time
            truncation_point = next(i for i, t in enumerate(history_new_primordia_times)
                                            if t > truncation_time)
            truncated_plastochrons = plastochrons[truncation_point:]
            truncated_divergence_angles = divergence_angles[truncation_point:]
            current_plastochrons_modes = list(get_modes(truncated_plastochrons,kernel_bandwidth=0.3))
            current_divergence_angles_modes = list(get_modes(truncated_divergence_angles,kernel_bandwidth=0.3))

            plastochrons_modes += current_plastochrons_modes
            divergence_angles_modes += current_divergence_angles_modes
            gammas_corresponding_to_plastochrons_modes += [gamma]*len(current_plastochrons_modes)
            gammas_corresponding_to_divergence_angles_modes += [gamma]*len(current_divergence_angles_modes)

    except OSError: # files not downloaded from onedrive will appear in the directory but not be readable, leading to an error
        pass

plt.scatter(gammas_corresponding_to_plastochrons_modes,plastochrons_modes)
plt.title("Plastochrons")
plt.xlabel("gamma")
plt.show()

plt.scatter(gammas_corresponding_to_divergence_angles_modes,divergence_angles_modes)
plt.title("Divergence angles")
plt.xlabel("gamma")
plt.show()