from matplotlib import pyplot as plt
import numpy as np
from numba import njit

# load state
def plot_potentials(primordia_positions,
                    conicity=3,
                    data_mode=False,
                    dt=0.125,
                    gamma=1.2,
                    meristem_radius=10,
                    overlay_active_ring=True,
                    overlay_positions=True,
                    potential_plot_max=2,
                    precompute_primordium_size_scaling_function_values=False,
                    primordium_size_scaling_function=None,
                    primordium_size_scaling_function_values=None,
                    primordia_sizes=None,
                    r_resolution=1000,
                    stiffness=3,
                    theta_resolution=1000):


    """
    data_mode - if True, the function returns the matrix of potential values instead of plotting them
    The function outputs a figure object that can be plotted or saved
    The figure output by the function is a heatmap of the potential function
    """
    
    alternating_primordia_sizes = primordia_sizes is not None
    age_dependent_primordium_size_scaling = (primordium_size_scaling_function is not None) or (
        (primordium_size_scaling_function_values is not None) and precompute_primordium_size_scaling_function_values)
    
    
    # these lines not only ensure the variables are arrays but also allow numba to access them as global variables
    primordia_positions = np.array(primordia_positions)
    primordia_sizes = np.array(primordia_sizes)
    if primordium_size_scaling_function_values is not None:
        primordium_size_scaling_function_values = np.array(primordium_size_scaling_function_values)
    
    meristem_growth_rate = 1
    primordium_radius = gamma * meristem_radius * (conicity ** -0.5)
    
    # space discretisation
    rmax = 100
    rs = np.linspace(0,rmax,r_resolution)
    thetas = np.linspace(0,2*np.pi,theta_resolution,endpoint=False)
    r_grid,theta_grid = np.meshgrid(rs,thetas)

        # define calculate_potential function
    if alternating_primordia_sizes: # define calculate_potential to take primordia_sizes as an input
        if age_dependent_primordium_size_scaling:
            if precompute_primordium_size_scaling_function_values: # calculate primordium_size_scaling_function_values array and access it in calculate_potential  
                # compute primordium_size_scaling_function_values if none have already been provided
                if primordium_size_scaling_function_values is None:
                    # evaluate primordium_size_scaling_function for every possible primordium age value, to be looked up by the calculate_potential function
                    primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function))
                    age_max = (rmax-meristem_radius)/meristem_growth_rate
                    ages = np.arange(0,age_max+dt,step=dt)
                    primordium_size_scaling_function_values = primordium_size_scaling_function_evaluated(ages).astype(np.float64) # must not be np.int32 type as this causes errors when attempting to store in JSON
                    
                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions,primordia_sizes): # primordia_positions must not be empty
                    potential = 0.0
                    for i in range(len(primordia_sizes)):
                        r2,theta2 = primordia_positions[i]
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        primordium_size = primordia_sizes[i]
                        age_index = int(primordium_age/dt)
                        scaled_primordium_size = primordium_size*primordium_size_scaling_function_values[age_index]
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential
            else: # call primordium_size_scaling_function_evaluated inside calculate_potential rather than precomputing the possible outputs
                primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function)) # njit is important to allow the function to be accessed by other njitted functions as well as improving performance
                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions,primordia_sizes): # primordia_positions must not be empty
                    potential = 0.0
                    for i in range(len(primordia_sizes)):
                        r2,theta2 = primordia_positions[i]
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        primordium_size = primordia_sizes[i]
                        scaled_primordium_size = primordium_size*primordium_size_scaling_function_evaluated(primordium_age)
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential
        else:
            @njit # decorator for numba just-in-time compiler in nopython mode
            def calculate_potential(r,theta,primordia_positions,primordia_sizes): # primordia_positions must not be empty
                potential = 0
                for i in range(len(primordia_sizes)):
                    r2,theta2 = primordia_positions[i]
                    primordium_size = primordia_sizes[i]
                    diff_theta = theta - theta2
                    d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                    if d != 0:
                        potential += (primordium_size/d)**stiffness
                    else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                        return 9999
                return potential
    else: # define calculate_potential to use primordium_radius instead of taking primordia_sizes as an input  
        if age_dependent_primordium_size_scaling: 
            if precompute_primordium_size_scaling_function_values: # calculate primordium_size_scaling_function_values array and access it in calculate_potential   
                # compute primordium_size_scaling_function_values if none have already been provided
                if primordium_size_scaling_function_values is None:
                    # evaluate primordium_size_scaling_function for every possible primordium age value, to be looked up by the calculate_potential function
                    primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function))
                    age_max = (rmax-meristem_radius)/meristem_growth_rate
                    ages = np.arange(0,age_max+dt,step=dt)
                    primordium_size_scaling_function_values = primordium_size_scaling_function_evaluated(ages).astype(np.float64) # must not be np.int32 type as this causes errors when attempting to store in JSON

                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions): # primordia_positions must not be empty
                    potential = 0.0
                    for r2,theta2 in primordia_positions:
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        age_index = int(primordium_age/dt)
                        scaled_primordium_size = primordium_radius*primordium_size_scaling_function_values[age_index]
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential

            else: # call primordium_size_scaling_function_evaluated inside calculate_potential rather than precomputing the possible outputs
                primordium_size_scaling_function_evaluated = njit(eval(primordium_size_scaling_function)) # njit is important to allow the function to be accessed by other njitted functions as well as improving performance
                @njit # decorator for numba just-in-time compiler in nopython mode
                def calculate_potential(r,theta,primordia_positions): # primordia_positions must not be empty
                    potential = 0.0
                    for r2,theta2 in primordia_positions:
                        primordium_age = (r2-meristem_radius)/meristem_growth_rate
                        scaled_primordium_size = primordium_radius*primordium_size_scaling_function_evaluated(primordium_age)
                        diff_theta = theta - theta2
                        d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                        if d != 0:
                            potential += (scaled_primordium_size/d)**stiffness
                        else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                            return 9999
                    return potential
        else: # simplest model

            @njit # decorator for numba just-in-time compiler in nopython mode
            def calculate_potential(r,theta,primordia_positions): # primordia_positions must not be empty
                potential = 0
                for r2,theta2 in primordia_positions:
                    diff_theta = theta - theta2
                    d = np.sqrt(conicity*(r-r2)**2 + 2*r*r2*(1-np.cos(diff_theta))/conicity)
                    if d != 0:
                        potential += (primordium_radius/d)**stiffness
                    else: # if a primordium is present at this exact point and d is 0, set the potential for this point to be 9999
                        return 9999
                return potential
    
    # evaluate potentials at each point in the space
    if alternating_primordia_sizes:
        potentials = np.array([[calculate_potential(r_grid[i,j],theta_grid[i,j],primordia_positions,primordia_sizes) 
                                for j in range(r_resolution)] 
                               for i in range(theta_resolution)])
    else:
        potentials = np.array([[calculate_potential(r_grid[i,j],theta_grid[i,j],primordia_positions)
                                for j in range(r_resolution)]
                               for i in range(theta_resolution)])
    
    if data_mode:
        return potentials
    else:
        # plotting

        # remove extreme values
        potentials[potentials>potential_plot_max] = None

        plt.subplot(projection="polar")
        fig = plt.pcolormesh(theta_grid,r_grid,potentials, cmap='Greens', vmin=0, vmax=potential_plot_max)
        plt.set_cmap('Greens')
        plt.colorbar(fig)
        plt.grid(False)
        plt.xticks([]) # hide theta axis tick labels
        plt.yticks([]) # hide r axis tick labels

        if overlay_active_ring:
            theta_meristem = np.linspace(0, 2*np.pi, 1000)
            r_meristem = meristem_radius*np.ones_like(theta_meristem)
            plt.plot(theta_meristem, r_meristem, color='grey', alpha=0.3, linewidth=1)
        
        if overlay_positions:
            for r,theta in primordia_positions:
                plt.plot(theta, r, marker='o', markersize=5, color="#00cc0a", markeredgecolor="none")
        
    return fig